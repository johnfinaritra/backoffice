app.controller('extraController', function($scope, $http, $location){
    if(getCookie()==null){
        $location.url('/');
	}
	
	$scope.initialize = function(){
		$scope.nom = "";
		$scope.tarif = "";
		$scope.remarque = "";
	};
	
	$scope.getExtrasReservation = function(idReservation){
		var path = wspath + '/infos/extras/'.idReservation;
		$http({method: 'GET', url: path })
		.then(function successCallback(response) {
			$scope.extrasReservation = response.data;
		},function errorCallback(error) {
			console.log(error);
		});
	};

    $scope.getExtras = function(){
		var path = wspath + '/infos/extras';
		$http({method: 'GET', url: path })
		.then(function successCallback(response) {
			$scope.extras = response.data;
		},function errorCallback(error) {
			console.log(error);
		});
	};

    $scope.modifierExtra = function(id, nom, tarif){
		$http({
			method : 'PUT',
			url : wspath+'/infos/modifier/'+ nom+"/"+id+"/"+tarif,
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : 'Bearer '+getCookie()
			}
		}).then(function successCallback(response) {
			alert(response.data);
		}, function errorCallback(error) {
			if(error.status==401){
				setCookie("token", "null");
				$location.url('/');
			}
			console.log(error);
		});
	};

	$scope.ajouterExtra = function (nom, tarif, remarque){
		var extra = {
			id : '',
			nom : nom,
			tarif : tarif,
			remarque : remarque
		};
		$http({
			method : 'POST',
			url : wspath+'/infos/ajoutextras',
			data : extra,
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : 'Bearer '+getCookie()
			}
		}).then(function successCallback(response) {
			alert(response.data);
		}, function errorCallback(error) {
			if(error.status==401){
				setCookie("token", "null");
				$location.url('/');
			}
			console.log(error);
		});
	};

});