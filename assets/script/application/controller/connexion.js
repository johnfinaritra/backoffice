var wspath = wspath;
app.controller('deconnectionController', function($location){
	setCookie("token", "null");
	$location.url('/');
});

app.controller('connexionController', function($scope, $http, $location){	
	if(getCookie()!=null){
        $location.url('/accueil');
    }
    $scope.connect = function(){
		var data = {
			id : '',
			nom : '',
			prenoms : '',
			datenaissance : '',
			sexe : '',
			email : $scope.email,
			motdepasse : $scope.motdepasse,
			contact : ''
		};
		$http({
			method : 'POST',
			url : wspath+'/user/connect',
			data : data,
			headers : {
				'Content-Type' : 'application/json'
			}
		}).then(function successCallback(response) {
			setCookie("token", response.data.token);
			$location.url('/accueil');
		}, function errorCallback(error) {
			console.log(error);
		});

		/*$http.post(wspath+'/user/connect', data).then(function successCallback(response){
			$location.url('/accueil');
		}, function errorCallback(error){
			alert(error.data);
		});*/
	};
});