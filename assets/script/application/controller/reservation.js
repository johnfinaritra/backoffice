app.controller('resaController', function($scope, $http, $location){
	if(getCookie()==null){
        $location.url('/');
	}

	$scope.getDetails = function(client, voyage){
		var path = wspath + '/reservation/details/'+client+'/'+voyage;
		$http({method: 'GET', url: path })
		.then(function successCallback(response) {
			$scope.reservations = null;
			/* Liste a finir */
			$scope.details = response.data;
		},function errorCallback(error) {
			console.log(error);
		});
	};

	$scope.ajouterReservation = function (client, place, voyage, remarque, vehicule){
		var reservation = {
			id : '',
			dateReservation : '',
			client : client,
			place : place,
			idvoyage : voyage,
			remarque : remarque,
			idvehicule : vehicule,
			etat : 1
		};
		$http({
			method : 'POST',
			url : wspath+'/reservation/ajout',
			data : reservation,
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : 'Bearer '+getCookie()
			}
		}).then(function successCallback(response) {
			alert(response.data);
		}, function errorCallback(error) {
			if(error.status==401){
				setCookie("token", "null");
				$location.url('/');
			}
			console.log(error);
		});
	};

	$scope.listeReservations = function(){
		var path = wspath + '/reservation/liste'
		$http({method: 'GET', url: path })
		.then(function successCallback(response) {
			$scope.reservations = response.data;
		},function errorCallback(error) {
			console.log(error);
		});
	};
	
	$scope.annulerReservation = function(id){
		$http({
			method : 'PUT',
			url : wspath+'/reservation/annuler/'+ id,
			data : voyage,
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : 'Bearer '+getCookie()
			}
		}).then(function successCallback(response) {
			alert(response.data);
		}, function errorCallback(error) {
			if(error.status==401){
				setCookie("token", "null");
				$location.url('/');
			}
			console.log(error);
		});
	};

});