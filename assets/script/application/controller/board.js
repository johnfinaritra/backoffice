var path = wspath;
app.controller('boardController', function($scope, $location, $http){
	if(getCookie()==null){
        $location.url('/');
    }
    $scope.reservationTrajets = function(){
		var path = wspath + '/trajet/stat';
		$http({method: 'GET', url: path })
		.then(function successCallback(response) {
			$scope.trajets = response.data;
			console.log(response.data);
		},function errorCallback(response) {
			console.log(response);
		});
	};
	$scope.labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
	$scope.series = ['Series A', 'Series B'];

	$scope.data = [
    [65, 59, 80, 81, 56, 55, 40],
    [28, 48, 40, 19, 86, 27, 90]
	];
});