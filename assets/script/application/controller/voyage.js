var wspath = wspath;
app.controller('voyageController', function($scope, $http, $location){
	if(getCookie()==null){
        $location.url('/');
    }
	$scope.initialize = function(){
		$scope.listeChauffeurs();
		$scope.listeVehicules();
		$scope.listeVoyages();
		$scope.listeTrajets();
	};

	$scope.listeTrajets = function(){
        var reservPath = wspath + '/trajet/liste';
		$http({method: 'GET', url: reservPath })
		.then(function successCallback(response) {
			$scope.trajets = response.data;
		},function errorCallback(response) {
			console.log(response);
		});
	};

	$scope.listeVehicules = function(){
		var path = wspath + '/infos/listevehicules';
		$http({method: 'GET', url: path })
		.then(function successCallback(response) {
			$scope.vehicules = response.data;
		},function errorCallback(response) {
			console.log(response);
		});
	};

	$scope.listeChauffeurs = function(){
		var path = wspath + '/infos/listechauffeurs';
		$http({method: 'GET', url: path })
		.then(function successCallback(response) {
			$scope.chauffeurs = response.data;
		},function errorCallback(response) {
			console.log(response);
		});
	};

	$scope.ajouterVoyage = function (datevoyage, heurevoyage, chauffeur, prix, trajet, voiture){
		var voyage = {
			id : '',
			daty : datevoyage,
			heure: heurevoyage + ':00',
			vehicule : voiture,
			chauffeur : chauffeur,
			tarif : prix,
			trajet : trajet
		};
		$http({
			method : 'POST',
			url : wspath+'/voyage/ajout',
			data : voyage,
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : 'Bearer '+getCookie()
			}
		}).then(function successCallback(response) {
			alert(response.data);
		}, function errorCallback(error) {
			if(error.status==401){
				setCookie("token", "null");
				$location.url('/');
			}
			console.log(error);
		});
		/*$http.post(wspath+'/voyage/ajout', voyage).then(function successCallback(response){
			alert(response.data);
		}, function errorCallback(error){
			console.log(error);
		});*/
	};

	$scope.annulerVoyage = function(id){
		$http({
			method : 'PUT',
			url : wspath+'/voyage/annuler/'+ id,
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : 'Bearer '+getCookie()
			}
		}).then(function successCallback(response) {
			alert(response.data);
			//$scope.listeVoyages();
		}, function errorCallback(error) {
			if(error.status==401){
				setCookie("token", "null");
				$location.url('/');
			}
			console.log(error);
		});
	};

	$scope.listeVoyages = function(){
		$scope.voyages = null;
		var path = wspath + '/voyage/liste';
		$http({method: 'GET', url: path })
		.then(function successCallback(response) {
			$scope.voyages = response.data;
		},function errorCallback(response) {
			console.log(response);
		});
	};
	
});