var wspath = wspath;
app.controller('vehiculeController', function($scope, $http, $location){
	if(getCookie()==null){
        $location.url('/');
    }
    $scope.listeVehicules = function(){
		var path = wspath + '/infos/listevehicules';
		$http({method: 'GET', url: path })
		.then(function successCallback(response) {
			$scope.vehicules = response.data;
		},function errorCallback(response) {
			console.log(response);
		});
    };
	
	$scope.initialize = function(){
		$scope.immatriculation = "";
		$scope.marque = "";
		$scope.places = "";
	};

	$scope.ajouterVehicule = function (immatriculation, marque, content){
		var vehicule = {
            id : immatriculation,
            marque : marque,
            capacite : content
		};
		$http({
			method : 'POST',
			url : wspath+'/infos/ajoutvehicule',
			data : vehicule,
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : 'Bearer '+getCookie()
			}
		}).then(function successCallback(response) {
			alert(response.data);
		}, function errorCallback(error) {
			if(error.status==401){
				setCookie("token", "null");
				$location.url('/');
			}
			alert(error.data);
		});
	};
});