app.controller('trajetController', function($scope, $http, $location){
	if(getCookie()==null){
        $location.url('/');
    }
	$scope.annuler = function(){
		$scope.depart = "";
		$scope.arrivee = "";
		$scope.distance = "";
		$scope.duree = "";
	};

    $scope.listeTrajets = function(){
		var path = wspath + '/trajet/liste';
		$http({method: 'GET', url: path })
		.then(function successCallback(response) {
			$scope.trajets = response.data;
		},function errorCallback(response) {
			console.log(response);
		});
    };
    
	$scope.ajouterTrajet = function (departure, arrival, dist, duration){
		var trajet = {
            id : '',
            depart : departure,
            arrivee : arrival,
            distance : dist,
            duree : duration
		};

		$http({
			method : 'POST',
			url : wspath+'/trajet/ajout',
			data : trajet,
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : 'Bearer '+getCookie()
			}
		}).then(function successCallback(response) {
			alert(response.data);
		}, function errorCallback(error) {
			if(error.status==401){
				setCookie("token", "null");
				$location.url('/');
			}
			console.log(error);
		});

		/*$http.post(wspath+'/trajet/ajout', trajet).then(function successCallback(response){
			alert(response.data);

		}, function errorCallback(error){
			alert(error.data);
		});*/
	};
})