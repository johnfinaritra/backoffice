
var app = angular.module('cotisseApp', ['ngRoute']);
var wspath = new Api().api;

// gestions des routes
app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when('/connexion', {
        templateUrl: './pages/connection.html',
        controller : 'connexionController'
        })
    .when('/reservation', {
        templateUrl : './pages/reservations.html',
        controller: 'resaController'
    })
    .when('/deconnection', {
        templateUrl : './pages/connection.html',
        controller: 'deconnectionController'
    })
    .when('/ExtraList', {
        templateUrl : './pages/ExtraList.html',
        controller: 'extraController'
    })
    .when('/Extra', {
        templateUrl : './pages/Extra.html',
        controller: 'extraController'
    })
    .when('/detailResa', {
        templateUrl : './pages/ficheReservation.html',
        controller: 'resaController'
    })
    .when('/voyage', {
        templateUrl : './pages/voyages.html',
        controller: 'voyageController'
    })
    .when('/voyageList', {
        templateUrl : './pages/voyagesList.html',
        controller: 'voyageController'
    })
    .when('/trajet', {
        templateUrl : './pages/trajets.html',
        controller: 'trajetController'
    })
    .when('/trajetList', {
        templateUrl : './pages/trajetsList.html',
        controller: 'trajetController'
    })
    .when('/accueil', {
        templateUrl : './pages/accueil.html',
        controller: 'accueilController'
    })
    .when('/vehicule', {
        templateUrl : './pages/vehicules.html',
        controller: 'vehiculeController'
    })
    .when('/vehiculeList', {
        templateUrl : './pages/vehiculeList.html',
        controller: 'vehiculeController'
    })
    .when('/dashboard', {
        templateUrl : './pages/statistiques.html',
        controller: 'boardController'
    })
    .when('/', {
        templateUrl: './pages/connection.html',
        controller : 'connexionController' // toute la page 1 ctrl
    })
    .when('/inscription', {
        templateUrl: './pages/inscription.html',
        //controller : 'connexionController' // toute la page 1 ctrl
    });
        /*$locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });*/
});

app.controller('cotisseController', function($scope, $http){
    $scope.urlNavbar = './pages/navbar.html';
});

app.controller('navCtrl', function($scope, $http){
  dropdown();
})

function dropdown(){
  /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
  var dropdown = document.getElementsByClassName("dropdown-btn");
  var i;
  for (i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var dropdownContent = this.nextElementSibling;
      if (dropdownContent.style.display === "block") {
        dropdownContent.style.display = "none";
      } else {
        dropdownContent.style.display = "block";
      }
    });
  }
}
